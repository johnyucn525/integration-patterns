package cn.johnyu.dubbo;

import cn.johnyu.dubbo.service.Greeter;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.bootstrap.DubboBootstrap;

public class Consumer {
    public static void main(String[] args) {
        ReferenceConfig<Greeter> referenceConfig=new ReferenceConfig();
        referenceConfig.setInterface(Greeter.class);
        referenceConfig.setRegistry(new RegistryConfig("zookeeper://localhost:2181"));
        DubboBootstrap.getInstance()
                .application("consumer")
                .reference(referenceConfig).start();
        Greeter greeter = referenceConfig.get();
        System.out.println(greeter.sayHello("john"));
    }
}

package cn.johnyu.dubbo;

import cn.johnyu.dubbo.service.Greeter;
import cn.johnyu.dubbo.service.GreeterImpl;
import org.apache.dubbo.config.ProtocolConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.ServiceConfig;
import org.apache.dubbo.config.bootstrap.DubboBootstrap;

public class Provider {
    public static void main(String[] args) throws Exception{
        Greeter greeter=new GreeterImpl();
        ServiceConfig<Greeter> serviceConfig = new ServiceConfig<>();
        serviceConfig.setInterface(Greeter.class);
        serviceConfig.setRef(greeter);
        DubboBootstrap.getInstance()
                .application("provider")
                .service(serviceConfig)
                .protocol(new ProtocolConfig("dubbo",-1))
                .registry(new RegistryConfig("zookeeper://localhost:2181"))
                .start();

        System.in.read();
    }
}

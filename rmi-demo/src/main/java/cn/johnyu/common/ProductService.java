package cn.johnyu.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

// 如果能成为远程方法，必须实现一个签名接口 Remote
public interface ProductService extends Remote {
    Product addProduct(String msg,int price);
}
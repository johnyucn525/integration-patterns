package cn.johnyu.service;

import cn.johnyu.common.ProductService;

import java.rmi.Naming;

// RMI 服务注册器
public class RmiRegister {
    public static void main(String[] args) throws Exception {
        ProductService helloService = new ProductServiceImpl();
        //向服务器进行注册
        Naming.bind("rmi://localhost:3001/helloService",helloService);
        System.out.println(helloService);
        System.out.println("helloService 注册完毕,注册地址： rmi://localhost:3001/helloService");

    }
}

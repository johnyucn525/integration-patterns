package cn.johnyu.service;

import cn.johnyu.common.ProductService;
import cn.johnyu.common.Product;

import java.io.Serializable;
import java.rmi.RemoteException;

//将以序列化机制（字节码方案），注册到RMI的注册中心
public class ProductServiceImpl implements ProductService,Serializable{
    @Override
    public Product addProduct(String msg, int price)  {
        Product product=null;
        try {
            product = new Product(msg, price);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return product;
    }
}
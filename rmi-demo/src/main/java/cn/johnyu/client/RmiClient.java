package cn.johnyu.client;

import cn.johnyu.common.ProductService;
import cn.johnyu.common.Product;

import java.rmi.Naming;

public class RmiClient {
    public static void main(String[] args) throws Exception{
        ProductService helloService = (ProductService) Naming.lookup("rmi://127.0.0.1:3001/helloService");
        Product product = helloService.addProduct("手机", 1000);

        System.out.println(product);
        System.out.println(helloService);
    }
}
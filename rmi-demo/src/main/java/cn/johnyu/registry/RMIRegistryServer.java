package cn.johnyu.registry;

import cn.johnyu.common.ProductService;
import cn.johnyu.service.ProductServiceImpl;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

// RMI 的注册中心
public class RMIRegistryServer {
    public static void main(String[] args) throws Exception{
        //启动一个RMI注册服务器(在子线程中启动)
        //此时此JVM 的任何应用，都可以把自身的某一个服务注册到： "rmi://ip:3001/[服务名]"，
        LocateRegistry.createRegistry(3001);
        //阻塞主线程，使用RMI注册服务器保持运行
        System.out.println("RMI 注册服务器启动完毕...");

//        ProductService helloService = new ProductServiceImpl();
//
//        //向服务器进行注册
//        Naming.bind("rmi://localhost:3001/helloService",helloService);
//        System.out.println("helloService 注册完毕！");

        new Scanner(System.in).next();
    }
}
package cn.johnyu.dubbo;

import cn.johnyu.dubbo.service.Greeter;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.bootstrap.DubboBootstrap;

public class Consumer {
    public static void main(String[] args) {
        ReferenceConfig<Greeter> referenceConfig=new ReferenceConfig();
        referenceConfig.setInterface(Greeter.class);
        referenceConfig.setUrl("dubbo://localhost:20880");
        DubboBootstrap.getInstance()
                .application("consumer")
                .reference(referenceConfig).start();

        Greeter greeter = referenceConfig.get();
        System.out.println(greeter.sayHello("john"));
    }
}

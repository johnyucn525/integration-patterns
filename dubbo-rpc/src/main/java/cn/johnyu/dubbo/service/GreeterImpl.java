package cn.johnyu.dubbo.service;

public class GreeterImpl implements Greeter{
    @Override
    public String sayHello(String name) {
        return "hello: "+name;
    }
}

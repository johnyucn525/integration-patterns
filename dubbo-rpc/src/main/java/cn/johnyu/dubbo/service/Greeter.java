package cn.johnyu.dubbo.service;

public interface Greeter  {
    String sayHello(String name);
}

package cn.johnyu;

import java.util.HashMap;
import java.util.Map;

public class LoginServer {
    private static Map<String,String> userDb=new HashMap<>();
    static {
        userDb.put("john","123");
        userDb.put("tom","234");
        userDb.put("alice","345");
    }

    public static String login(String username,String password){
        if(password.equals(userDb.get(username))){
            return username;
        }
        else{
            return null;
        }
    }
}

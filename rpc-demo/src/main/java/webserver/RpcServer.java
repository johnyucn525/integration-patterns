package webserver;
 
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.webserver.WebServer;

import java.util.HashMap;
import java.util.Map;

public class RpcServer {
    private static final int port = 9800;

    public static void main(String[] args) throws Exception {

        //webServer 扮演 http的连接器，服务形式： http://ip:port/XML_RPC_Server/service
        WebServer webServer = new WebServer(port);

        //xmlRpcServer 扮演RPC 的注册中心
        XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();
        PropertyHandlerMapping phm = new PropertyHandlerMapping();


        Map<String,String> map=new HashMap<>();
        map.put("Calculator","service.Calculator"); //硬编码方式
        phm.load(Thread.currentThread().getContextClassLoader(),map);
        //配置方式
//        phm.load(Thread.currentThread().getContextClassLoader(), "MyHandlers.properties");

        //注册服的核心方法
        xmlRpcServer.setHandlerMapping(phm);

        webServer.start();//启动服务
        System.out.println("服务已经成功的在"+port+" 地址： http://localhost:9800/XML_RPC_Server/service");
    }
}